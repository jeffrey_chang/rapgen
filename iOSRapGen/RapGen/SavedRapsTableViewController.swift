//
//  SavedRapsTableViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/20/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit
import CoreData

class SavedRapsTableViewController: UITableViewController {

    let settings = SettingsModel()
    var appDelegate: AppDelegate!
    var managedContext: NSManagedObjectContext!
    var savedRaps = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Raps"

        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "TITLECELL")
        
        tableView.separatorColor = settings.darkPurple
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let fetchRequest = NSFetchRequest(entityName: "SavedRap")
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            savedRaps = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        tableView.reloadData()
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedRaps.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("LYRICSSEGUE", sender: indexPath);
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TITLECELL", forIndexPath: indexPath)

        let savedRap = savedRaps[savedRaps.count - indexPath.row - 1]
        
        cell.textLabel!.text = savedRap.valueForKey("title") as? String
        cell.textLabel!.font = UIFont.systemFontOfSize(24.0)
        cell.textLabel!.textColor = settings.darkPurple
        //cell.backgroundColor = settings.lightPurple
        cell.accessoryType = .DisclosureIndicator

        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            do {
                managedContext.deleteObject(savedRaps[savedRaps.count - indexPath.row - 1])
                savedRaps.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                try managedContext.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "LYRICSSEGUE" {
            let path = sender as! NSIndexPath
            let rapObject = savedRaps[savedRaps.count - path.row - 1]
            let title = rapObject.valueForKey("title")
            let lyrics = rapObject.valueForKey("lyrics")

            let toSend = "\(title!)\n\n\(lyrics)"
            
            if let destVC = segue.destinationViewController as? ViewRapViewController {
                    destVC.lyrics = toSend
            }
        }
    }

}
