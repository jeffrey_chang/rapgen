//
//  RGConnection.swift
//  RapGen
//
//  Created by Timothy Lin on 8/6/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class RGConnection: NSObject, NSStreamDelegate {
    var inputStream: NSInputStream?
    var outputStream: NSOutputStream?
    let port = 8000
    //let address = "52.88.48.169"
    let address = "165.123.201.17"
    var delegate:RGConnectionDelegate?
    var lineNum = 10
    var linesReceived = 0
    
    override init() {
        super.init()
    }
    
    func generate(seedLine: String, clean: Bool, highFactor: Float, rapType: String) {
        
        autoreleasepool {
            NSStream.getStreamsToHostWithName(address, port: port, inputStream: &inputStream , outputStream: &outputStream)
        }
        
        inputStream!.delegate = self
        outputStream!.open()
        
        let string = "GIMME A RAP\n\(seedLine)\n\(clean) \(highFactor) \(rapType)\n"
        print(string)
        let buffer: [UInt8] = Array(string.utf8)
        // Here you can write() to `outputStream`
        outputStream!.write(buffer, maxLength: buffer.count)
        
        inputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
        inputStream!.open()
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        if aStream === inputStream! {
            switch eventCode {
            case NSStreamEvent.ErrorOccurred:
                print("input: ErrorOccurred: \(aStream.streamError?.description)")
            case NSStreamEvent.OpenCompleted:
                print("input: OpenCompleted")
            case NSStreamEvent.HasBytesAvailable:
                var buffer = [UInt8](count: 4096, repeatedValue: 0)
                
                while inputStream!.hasBytesAvailable {
                    let len = inputStream!.read(&buffer, maxLength: buffer.count)
                    if(len > 0){
                        let output = NSString(bytes: &buffer, length: buffer.count, encoding: NSUTF8StringEncoding)
                        if (output != "") {
                            print("server said: %@", output!)
                            if let newLyrics = output {
                                let newLyricsStr = newLyrics as String
                                if newLyricsStr.hasPrefix("HERES YO RAP DAWG") {
                                    let lineNumStr = newLyricsStr.substringWithRange(Range<String.Index>(start: newLyricsStr.startIndex.advancedBy(23), end: newLyricsStr.endIndex.advancedBy(-6)))
                                    let lineNum = Int(lineNumStr)
                                    if let lineNums = lineNum {
                                        self.lineNum = lineNums
                                    }
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), {
                                        if let delegate = self.delegate {
                                            delegate.updateLyrics("\(newLyrics)")
                                            delegate.updateProgress(Float(self.linesReceived)/Float(self.lineNum))
                                        }
                                        self.linesReceived = self.linesReceived + 1
                                    })
                                }
                            }
                        }
                    }
                }

            case NSStreamEvent.EndEncountered:
                print("end connection")
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let delegate = self.delegate {
                        delegate.updateProgress(1.0)
                        delegate.postLyrics()
                    }
                })

                endConnection()
                
            default:
                print("something on input happened")
                break
            }
        } else {
            print("shouldn't be here")
        }
    }
    
    func cancel() {
        outputStream!.open()
        let string = "JK"
        print(string)
        let buffer: [UInt8] = Array(string.utf8)
        inputStream!.removeFromRunLoop(.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        outputStream!.write(buffer, maxLength: buffer.count)
        endConnection()
    }
    
    func endConnection() {
        linesReceived = 0
        inputStream!.removeFromRunLoop(.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        inputStream!.close()
        inputStream = nil
        outputStream!.close()
        outputStream = nil
    }

}

protocol RGConnectionDelegate {
    func updateLyrics(newLyrics: String)
    func postLyrics()
    func updateProgress(newProgress: Float)
}
