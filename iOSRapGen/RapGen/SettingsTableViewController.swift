//
//  SettingsTableViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/14/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    var settings:SettingsModel!
    @IBOutlet weak var rapTypeLabel: UILabel!
    @IBOutlet weak var highFactorLabel: UILabel!
    @IBOutlet weak var censorSwitch: UISwitch!
    
    //label outlets to change colors
    @IBOutlet weak var cellLabel1: UILabel!
    @IBOutlet weak var cellLabel2: UILabel!
    @IBOutlet weak var cellLabel3: UILabel!
    @IBOutlet weak var cellLabel4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"
        
        //get settings model
        let tbc = tabBarController as! RapGenUITabBarController
        settings = tbc.settings
        
        cellLabel1.textColor = settings.darkPurple
        cellLabel2.textColor = settings.darkPurple
        cellLabel3.textColor = settings.darkPurple
        cellLabel4.textColor = settings.darkPurple
        
        tableView.separatorColor = settings.darkPurple
        
        //removes empty table cells
        tableView.tableFooterView = UIView()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    override func viewWillAppear(animated: Bool) {
        //set censorswitch bar
        if settings.censor {
            censorSwitch.on = true
        } else {
            censorSwitch.on = false
        }
        
        //change label
        rapTypeLabel.text = settings.rapType
        highFactorLabel.text = settings.calculateHigh(settings.highFactor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }

    @IBAction func censorSwitched(sender: UISwitch) {
        if(sender.on) {
            settings.censor = true
        } else {
            settings.censor = false
        }
        settings.save()
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //cell.backgroundColor = settings.lightPurple
    }

    /* override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        return cell
    } */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
