//
//  ContactUsViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 8/30/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var contactUsButton: UIButton!
    
    override func viewDidLoad() {
        let tbc = tabBarController as! RapGenUITabBarController
        let settings = tbc.settings
        contactUsButton.tintColor = settings.darkPurple
    }
    
    @IBAction func contactUsPressed(sender: UIButton) {
        let email = "rapgenerator@gmail.com"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
    }
}
