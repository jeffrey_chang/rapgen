//
//  SetSettingsTableViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/16/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class SetSettingsTableViewController: UITableViewController {
    
    //if this is 3, then I know this is the first time this page is being loaded
    var selectedIndex = 3
    
    var settings:SettingsModel!
    
    let labelNames = ["Dr. Dre", "Kendrick Lamar", "Drake", "Eminem", "Kanye West", "The Notorious B.I.G."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get settings model
        let tbc = tabBarController as! RapGenUITabBarController
        settings = tbc.settings
        
        //removes empty table cells
        tableView.tableFooterView = UIView()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelNames.count
    }
    
    //code for generic tablecell checkmark/selection
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.row
        
        settings.rapType = labelNames[selectedIndex]
        settings.save()
        tableView.reloadData()
    }
    
    /*override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    }*/

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RAPTYPECELL", forIndexPath: indexPath)

        //set label name of this cell
        cell.textLabel?.text = labelNames[indexPath.row]
        cell.textLabel?.textColor = settings.darkPurple
        
        print("yo the index is: \(selectedIndex)")
        
        //if this is first time this page is opened
        selectedIndex = 0
        var i = 0
        while i < labelNames.count {
            if settings.rapType == labelNames[i] {
                selectedIndex = i
                break
            }
            i = i + 1
        }
        
        // Configure the cell...
        if indexPath.row == selectedIndex {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
