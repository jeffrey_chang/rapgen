//
//  RapGenUITabBarController.swift
//  RapGen
//
//  Created by Timothy Lin on 4/17/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit
import FontAwesome_swift

class RapGenUITabBarController: UITabBarController {

    let settings = SettingsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mic = UIImage.fontAwesomeIconWithName(.Microphone, textColor: settings.gold, size: CGSizeMake(30, 30))
        let cog = UIImage.fontAwesomeIconWithName(.Cog, textColor: settings.gold, size: CGSizeMake(30, 30))
        let book = UIImage.fontAwesomeIconWithName(.Book, textColor: settings.gold, size: CGSizeMake(30, 30))
        
        
        let tabBarItems = tabBar.items! as [UITabBarItem]
        
        UITabBar.appearance().tintColor = settings.gold
        UITabBar.appearance().barTintColor = settings.darkPurple
        
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: settings.gold], forState: .Normal)
        
        tabBarItems[0].image = book.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItems[0].title = "My Raps"
        tabBarItems[1].image = mic.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItems[1].title = "RapGen"
        tabBarItems[2].image = cog.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItems[2].title = "Settings"
        
        self.selectedIndex = 1;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
