//
//  NavViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 7/2/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class NavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get settings model
        let tbc = tabBarController as! RapGenUITabBarController
        let settings = tbc.settings
        
        navigationBar.barTintColor = settings.darkPurple
        navigationBar.tintColor = settings.gold
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: settings.gold]


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
