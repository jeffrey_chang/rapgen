//
//  ViewRapViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/20/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class ViewRapViewController: UIViewController {

    let settings = SettingsModel()
    var lyrics:String?
    @IBOutlet weak var lyricsTV: UITextView!
    @IBOutlet weak var shareBar: UIToolbar!
    @IBOutlet weak var shareButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lyricsTV.text = lyrics
        lyricsTV.textAlignment = .Center
        lyricsTV.font = UIFont.systemFontOfSize(20.0)
        lyricsTV.textColor = settings.darkPurple
        
        shareBar.barTintColor = UIColor.whiteColor()
        shareBar.tintColor = settings.darkPurple
        shareBar.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sharePressed(sender: UIButton) {
        
        let shareImage = UIImage(named: "RG Share Final.png")
        
        UIGraphicsBeginImageContextWithOptions(lyricsTV.contentSize, lyricsTV.opaque, 0.0)
        
        let savedContentOffset: CGPoint = lyricsTV.contentOffset
        let savedFrame: CGRect = lyricsTV.frame
        
        lyricsTV.contentOffset = CGPointZero
        lyricsTV.frame = CGRectMake(0, 0, lyricsTV.contentSize.width, lyricsTV.contentSize.height)
        
        lyricsTV.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let lyricsImage = UIGraphicsGetImageFromCurrentImageContext()
        
        lyricsTV.contentOffset = savedContentOffset
        lyricsTV.frame = savedFrame
        
        UIGraphicsEndImageContext()
        
        let objectsToShare = [shareImage, lyricsImage]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender
        self.presentViewController(activityVC, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
