//
//  SetSettingsViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/17/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit

class SetSettingsViewController: UIViewController {

    @IBOutlet weak var highSlider: UISlider!
    @IBOutlet weak var highFactorLabel: UILabel!
    var settings:SettingsModel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        //get settings model
        let tbc = tabBarController as! RapGenUITabBarController
        settings = tbc.settings
        
        highSlider.tintColor = settings.gold
        
        highFactorLabel.text = settings.calculateHigh(settings.highFactor)
        highFactorLabel.textColor = settings.darkPurple
        highSlider.value = settings.highFactor
        if settings.calculateHigh(settings.highFactor) == "Wasted" {
            highFactorLabel.textColor = UIColor.redColor()
        } else {
            highFactorLabel.textColor = settings.gold
        }
        
    }

    @IBAction func sliderChanged(sender: UISlider) {
        settings.highFactor = sender.value
        settings.save()
        
        //DEBUG
        //highFactorLabel.text = String(settings.scaleHigh(settings.highFactor))
        highFactorLabel.text = settings.calculateHigh(sender.value)
        if settings.calculateHigh(sender.value) == "Wasted" {
            highFactorLabel.textColor = UIColor.redColor()
            highSlider.tintColor = UIColor.redColor()
        } else {
            highFactorLabel.textColor = settings.gold
            highSlider.tintColor = settings.gold
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
