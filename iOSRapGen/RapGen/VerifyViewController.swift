//
//  VerifyViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 6/20/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit
import CoreData

class VerifyViewController: UIViewController {
    
    //MARK: Properties
    let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    var dataTask: NSURLSessionDataTask?
    
    //CoreData
    var appDelegate: AppDelegate!
    var managedContext: NSManagedObjectContext!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var errorTV: UITextView!
    @IBOutlet weak var djankCode: UIButton!
    
    var button:UIButton?
    
    let link = "http://54.213.216.154:8000/verify/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //djank shit to make the segue work
        djankCode.enabled = false
        djankCode.hidden = true
        
        //CoreData
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Verification")
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let verifications = results as! [NSManagedObject]
            if verifications.count > 0 {
                remember(verifications[0].valueForKey("name") as! String, code: verifications[0].valueForKey("code") as! String)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func remember(name: String, code: String) {
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let expectedCharSet = NSCharacterSet.URLQueryAllowedCharacterSet()
        
        var urlString = link
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Minute, .Hour, .Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        
        let time = "\(month)-\(day)-\(year)-\(hour)-\(minute)"
        
        let urlname = name.stringByAddingPercentEncodingWithAllowedCharacters(expectedCharSet)
        let urlcode = code.stringByAddingPercentEncodingWithAllowedCharacters(expectedCharSet)
        urlString = "\(urlString)?name=\(urlname!)&code=\(urlcode!)&insta=true&time=\(time)"
        print(urlString)
    
        let url = NSURL(string: urlString)
        
        //debug
        print(url?.absoluteString)
        
        dataTask = defaultSession.dataTaskWithURL(url!) {
            data, response, error in
            
            print("performing task")
            
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            if let error = error {
                print("Error:")
                print(error.localizedDescription)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    self.verifyReceive(data, fromButton: false)
                }
            }
            
        }
        dataTask?.resume()

    }
    
    @IBAction func verify(sender: UIButton) {
        
        button = sender
        
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let expectedCharSet = NSCharacterSet.URLQueryAllowedCharacterSet()
        
        var urlString = link
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Minute, .Hour, .Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        
        let time = "\(month)-\(day)-\(year)-\(hour)-\(minute)"
        
        if let newName = nameTF.text {
            if let newCode = codeTF.text {
                let name = newName.stringByAddingPercentEncodingWithAllowedCharacters(expectedCharSet)
                let code = newCode.stringByAddingPercentEncodingWithAllowedCharacters(expectedCharSet)
                urlString = "\(urlString)?name=\(name!)&code=\(code!)&insta=false&time=\(time)"
                print(urlString)
            }
        } else {
            print("Error: No name/code")
            return
        }

        let url = NSURL(string: urlString)
        
        //debug
        print(url?.absoluteString)
        
        dataTask = defaultSession.dataTaskWithURL(url!) {
            data, response, error in
            
            print("performing task")
            
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            if let error = error {
                print("Error:")
                print(error.localizedDescription)
                dispatch_async(dispatch_get_main_queue()) {
                    let alert = UIAlertController(title: "Cannot Verify Code",
                        message: "No internet connection is available",
                        preferredStyle: .Alert)
                    
                    let okAction = UIAlertAction(title: "OK",
                        style: .Default) { (action: UIAlertAction) -> Void in
                    }
                    
                    alert.addAction(okAction)
                    
                    self.presentViewController(alert,
                        animated: true,
                        completion: nil)
                }
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    self.verifyReceive(data, fromButton: true)
                }
            }
            
        }
        dataTask?.resume()
    }
    
    func verifyReceive(code: NSData?, fromButton: Bool) {
        do {
            if let data = code, response = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue:0)) as? [String: AnyObject] {
                
                if let isGood: AnyObject = response["good"] {
                    if let isGood = isGood as? Bool {
                        dispatch_async(dispatch_get_main_queue()) {
                            if isGood {
                                if fromButton {
                                    self.save(self.nameTF.text!, code: self.codeTF.text!)
                                }
                                self.performSegueWithIdentifier("VERIFYSEGUE", sender: self.button)
                            } else {
                                if fromButton {
                                    self.errorTV.text = "Incorrect verification credentials. Please try again."
                                    self.errorTV.textColor = UIColor.redColor()
                                }
                            }
                        }
                    } else {
                        print("Error: Response was not a string")
                    }
                }
                
            } else {
                print("Error: JSON Error")
            }
        } catch let error as NSError {
            print("Error in parsing results: \(error.localizedDescription)")
        }
    }
    
    func save(name: String, code: String) {
        let entity =  NSEntityDescription.entityForName("Verification",
            inManagedObjectContext:managedContext)
        
        let verification = NSManagedObject(entity: entity!,insertIntoManagedObjectContext: managedContext)
        
        verification.setValue(code, forKey: "code")
        verification.setValue(name, forKey: "name")
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
