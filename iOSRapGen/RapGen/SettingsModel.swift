//
//  SettingsModel.swift
//  RapGen
//
//  Created by Timothy Lin on 6/14/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit
import CoreData

class SettingsModel: NSObject {
    
    var appDelegate: AppDelegate
    var managedContext: NSManagedObjectContext
    var settings: NSManagedObject?
    
    var censor: Bool
    var rapType: String
    var highFactor: Float
    let highDictionary = ["Wasted", "Stoned", "In the Zone", "Detox", "Rehabbed"]
    
    //colors throughout the app
    let lightPurple = UIColor(red: 74/255, green: 35/255, blue: 90/255, alpha: 1.0) /* #4a235a */
    //let lightPurple = UIColor(red: 195/255, green: 157/255, blue: 221/255, alpha: 1.0) /* #c39ddd */
    let darkPurple = UIColor(red: 33/255, green: 12/255, blue: 42/255, alpha: 1.0) /* #210c2a */
    let gold = UIColor(red: 241/255, green: 196/255, blue: 15/255, alpha: 1.0) /* #f1c40f */
    
    override init() {
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext

        censor = true
        rapType = "Dr. Dre"
        highFactor = 0.5
        
        super.init()        
    }
    
    //jeffrey does some math
    func scaleHigh(highFactor: Float) -> Float {
        if highFactor <= 0.5 {
            return 0.1 + 1.2 * highFactor
        } else {
            return -0.3 + 2.0 * highFactor
        }
    }
    
    func calculateHigh(highFactor: Float) -> String {
        if highFactor >= 0.8 {
            return highDictionary[0]
        } else if highFactor >= 0.6 {
            return highDictionary[1]
        } else if highFactor >= 0.4 {
            return highDictionary[2]
        } else if highFactor >= 0.2 {
            return highDictionary[3]
        } else {
            return highDictionary[4]
        }
    }
    
    //this should be done only once (it is done in fetch if no model currently exists)
    func createNew() {
        let entity =  NSEntityDescription.entityForName("Settings",
            inManagedObjectContext:managedContext)
        
        settings = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        if let set = settings {
            set.setValue(censor, forKey: "censor")
            set.setValue(rapType, forKey: "rapType")
            set.setValue(highFactor, forKey: "highFactor")
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func save() {
        if let set = settings {
            set.setValue(censor, forKey: "censor")
            set.setValue(rapType, forKey: "rapType")
            set.setValue(highFactor, forKey: "highFactor")
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func fetch() {
        let fetchRequest = NSFetchRequest(entityName: "Settings")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let settingsArr = results as! [NSManagedObject]
            print("array count: " + String(settingsArr.count))
            
            if settingsArr.count == 1 {
                settings = settingsArr[0]
                if let set = settings {
                    censor = set.valueForKey("censor") as! Bool
                    rapType = set.valueForKey("rapType") as! String
                    highFactor = set.valueForKey("highFactor") as! Float
                } else {
                    print("Error: settingsArr does not exist")
                }
            } else if settingsArr.count == 0 {
                //this is where it is done once
                createNew()
            } else {
                print("Error: settingsArr count is more than 1")
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
}
