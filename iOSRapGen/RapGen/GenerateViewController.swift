//
//  FirstViewController.swift
//  RapGen
//
//  Created by Timothy Lin on 3/20/16.
//  Copyright © 2016 Lin Enterprises. All rights reserved.
//

import UIKit
import CoreData
import PlainPing
import Foundation

class GenerateViewController: UIViewController, UITextFieldDelegate, RGConnectionDelegate {
    
    //MARK: Properties
    //connection elements
    let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    var dataTask: NSURLSessionDataTask?
    //let address = "52.88.48.169"
    let SERVER_IP = "165.123.201.17"
    
    //Settings
    var settings: SettingsModel!
    
    //CoreData
    var appDelegate: AppDelegate!
    var managedContext: NSManagedObjectContext!
    var savedRaps = [NSManagedObject]()
    
    //storyboard elements
    @IBOutlet weak var seedLineTF: UITextField!
    @IBOutlet weak var lyricsTV: UITextView!
    @IBOutlet weak var saveRapButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var shareBar: UIToolbar!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet var genView: UIView!
    
    //seed line to generate from
    var seedLine: String?
    
    let connection = RGConnection()
    var lyrics: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get settings model
        let tbc = tabBarController as! RapGenUITabBarController
        settings = tbc.settings
        
        // Do any additional setup after loading the view, typically from a nib.
        //CoreData
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        //fetch settings from CoreData
        settings.fetch()
        
        connection.delegate = self
        
        initializeUI()
    }
    
    
    //MARK: UI Code
    func initializeUI() {
        //shareBar initialization
        shareBar.barTintColor = UIColor.whiteColor()
        shareBar.tintColor = settings.darkPurple
        shareBar.hidden = true
        shareBar.userInteractionEnabled = false
        shareBar.clipsToBounds = true
        
        //handle text field's user input through delegate callbacks
        seedLineTF.delegate = self
        lyricsTV.text = ""
        
        //Color
        seedLineTF.borderStyle = .RoundedRect
        //seedLineTF.layer.borderWidth = 1
        //seedLineTF.layer.borderColor = settings.darkPurple.CGColor
        seedLineTF.attributedPlaceholder = NSAttributedString(string:"Enter a sick line dawg", attributes:[NSForegroundColorAttributeName: settings.lightPurple, NSFontAttributeName: UIFont.systemFontOfSize(24.0)])
        //seedLineTF.backgroundColor = settings.darkPurple
        seedLineTF.textColor = settings.darkPurple
        
        lyricsTV.text = ""
        
        let newTextViewHeight = ceil(lyricsTV.sizeThatFits(lyricsTV.frame.size).height)
        lyricsTV.frame.size.height = newTextViewHeight
        
        lyricsTV.textColor = settings.darkPurple
        lyricsTV.textAlignment = .Center
        lyricsTV.font = UIFont.systemFontOfSize(20.0)
        
        //keyboard key type
        seedLineTF.returnKeyType = .Done
        //deals with keyboard closing
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //UITextFieldDelegate function to make seedLineTF pretty
    override func viewDidLayoutSubviews() {
        //set border for seedLineTF
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = settings.darkPurple.CGColor
        border.frame = CGRect(x: 0, y: seedLineTF.frame.size.height - width, width:  seedLineTF.frame.size.width, height: seedLineTF.frame.size.height)
        
        border.borderWidth = width
        seedLineTF.layer.addSublayer(border)
        seedLineTF.layer.masksToBounds = true
    }
    
    //UITextFieldDelegate function. called when user taps keyboard's return button
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        //clear for next generation
        lyricsTV.text = ""
        lyricsTV.editable = false
        shareBar.userInteractionEnabled = false
        shareBar.hidden = true
        self.alertView?.dismissViewControllerAnimated(false, completion: nil)
        
        //set seedline and generate
        seedLine = textField.text
        generate()
        
        //hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    
    //MARK: Generate Code
    func generate() {
        if let seedLine = self.seedLineTF.text {
            let charSet = NSCharacterSet(charactersInString: " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~")
            if seedLine.rangeOfCharacterFromSet(charSet) == nil {
                self.alertView?.dismissViewControllerAnimated(true, completion: nil)
                self.displayError(.Char)
                return
            }
        } else {
            self.alertView?.dismissViewControllerAnimated(true, completion: nil)
            self.displayError(.Error)
        }
        
        //check if internet is available
        let reachability:Reachability = Reachability()
        if !reachability.isConnectedToNetwork() {
            displayError(.NoInternet)
        }
        
        //progress bar
        displayProgress()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        //ping server
        PlainPing.ping(SERVER_IP, completionBlock: { (timeElapsed:Double?, error:NSError?) in
            if error != nil {
                self.alertView?.dismissViewControllerAnimated(true, completion: nil)
                self.displayError(.ServerDown)
                return
            } else {
                if let seedLine = self.seedLineTF.text {
                    dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.rawValue), 0), {
                        self.lyrics = ""
                        let processedHighFactor = self.settings.scaleHigh(self.settings.highFactor)
                        self.connection.generate(seedLine, clean: self.settings.censor, highFactor: processedHighFactor, rapType: self.settings.rapType)
                    })
                } else {
                    self.alertView?.dismissViewControllerAnimated(true, completion: nil)
                    self.displayError(.Error)
                }
            }
        })

    }
 
    //hacky way to replace newlines with " / ". the reason is because the string received from the inputstream doesn't seem to follow regular string encoding and was causing issues
    func updateLyrics(newLyrics: String) {
        dispatch_async(dispatch_get_main_queue(), {
            if self.lyrics == "" {
                self.lyrics = newLyrics
            } else {
                let processedLyrics = newLyrics.stringByReplacingOccurrencesOfString("\n", withString: "")
                self.lyrics = processedLyrics + " / " + self.lyrics
            }
        })
    }
    
    func postLyrics() {
        dispatch_async(dispatch_get_main_queue()) {
            if (self.lyrics.rangeOfString("BRUH YOU GOOFED") != nil && self.lyrics.rangeOfString("\(self.settings.rapType)") != nil) {
                self.alertView?.dismissViewControllerAnimated(true, completion: {
                    self.displayError(.Artist)
                })
            } else if(self.lyrics.rangeOfString("BRUH YOU GOOFED") != nil) {
                self.alertView?.dismissViewControllerAnimated(true, completion: {
                    self.displayError(.Error)
                })
            } else if(self.lyrics.rangeOfString("DAMMIT") != nil) {
                self.alertView?.dismissViewControllerAnimated(true, completion: {
                    self.displayError(.ServerOverload)
                })
            } else {
                self.alertView?.dismissViewControllerAnimated(true, completion: nil)
                print(self.lyrics)

                self.lyricsTV.text = self.lyrics
                self.lyricsTV.textAlignment = .Center
                self.lyricsTV.textColor = self.settings.darkPurple
                self.lyricsTV.font = UIFont.systemFontOfSize(20.0)
                
                self.shareBar.userInteractionEnabled = true
                self.shareBar.hidden = false
                self.seedLineTF.userInteractionEnabled = true
                self.seedLineTF.resignFirstResponder()
            }
            self.lyrics = ""
        }
    }
    
    //MARK: Save
    @IBAction func savePressed(sender: UIButton) {    let alert = UIAlertController(title: "New Rap",
            message: "Name This Rap",
            preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save",
            style: .Default,
            handler: { (action:UIAlertAction) -> Void in
                
                let textField = alert.textFields!.first
                self.save(textField!.text!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
            style: .Default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (textField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert,
            animated: true,
            completion: nil)
    }
    
    func save(rapName: String) {
        let entity =  NSEntityDescription.entityForName("SavedRap",
            inManagedObjectContext:managedContext)
        
        let savedRap = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        if rapName.isEmpty {
            savedRap.setValue("Untitled", forKey: "title")
        } else {
            savedRap.setValue(rapName, forKey: "title")
        }
        
        savedRap.setValue(lyricsTV.text, forKey: "lyrics")
        //savedRap.setValue("The theme of the work is a Socratic question previously explored in the works of Plato, Aristotle's friend and teacher, of how men should best live. In his Metaphysics, Aristotle described how Socrates, the friend and teacher of Plato, had turned philosophy to human questions, whereas Pre-Socratic philosophy had only been theoretical. Ethics, as now separated out for discussion by Aristotle, is practical rather than theoretical, in the original Aristotelian senses of these terms.[1] In other words, it is not only a contemplation about good living, because it also aims to create good living. It is therefore connected to Aristotle's other practical work, the Politics, which similarly aims at people becoming good. Ethics is about how individuals should best live, while the study of politics is from the perspective of a law-giver, looking at the good of a whole community.", forKey: "lyrics")
        
        do {
            try managedContext.save()
            savedRaps.append(savedRap)
            print("the lyrics just saved were: \(savedRap.valueForKey("lyrics"))")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    //MARK: Share
    @IBAction func sharePressed(sender: UIButton) {
        
        let shareImage = UIImage(named: "RG Share Final.png")
        
        UIGraphicsBeginImageContextWithOptions(lyricsTV.contentSize, lyricsTV.opaque, 0.0)
        
        let savedContentOffset: CGPoint = lyricsTV.contentOffset
        let savedFrame: CGRect = lyricsTV.frame
        
        lyricsTV.contentOffset = CGPointZero
        lyricsTV.frame = CGRectMake(0, 0, lyricsTV.contentSize.width, lyricsTV.contentSize.height)
        
        lyricsTV.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let lyricsImage = UIGraphicsGetImageFromCurrentImageContext()
        
        lyricsTV.contentOffset = savedContentOffset
        lyricsTV.frame = savedFrame
        
        UIGraphicsEndImageContext()
        
        let objectsToShare = [shareImage, lyricsImage]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender
        self.presentViewController(activityVC, animated: true, completion: nil)
    }
    
    
    @IBAction func editPressed(sender: UIButton) {
        if lyricsTV.editable == true {
            lyricsTV.editable = false
            lyricsTV.textAlignment = .Center
            editButton.tintColor = settings.darkPurple
        } else {
            lyricsTV.editable = true
            lyricsTV.textAlignment = .Center
            editButton.tintColor = settings.gold
        }
    }
    
    //MARK: Progress Message
    //update progress in progress bar
    //MARK: - PROGRESS/COMPLETION
    //alertView to show user upload results
    //progressView to show user upload progress
    var alertView: UIAlertController?
    var progressView: UIProgressView?
    
    //display upload progress with progress bar
    func displayProgress() {
        //set alertView's properties
        alertView = UIAlertController(title: "Generating Rap...", message: "", preferredStyle: .Alert)
        //add cancel button to alertView that tells transferManager to cancel task
        alertView!.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: {(alert: UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue(), {
                self.seedLineTF.resignFirstResponder()
                self.connection.cancel()
            })
        }))
        
        //  Show it to your users
        presentViewController(alertView!, animated: true, completion: {
            //  Add your progressbar after alert is shown (and measured)
            let margin:CGFloat = 8.0
            let rect = CGRectMake(margin, 72.0, self.alertView!.view.frame.width - margin * 2.0 , 2.0)
            self.progressView = UIProgressView(frame: rect)
            self.progressView!.progress = 0
            self.progressView!.tintColor = UIColor.blueColor()
            self.alertView!.view.addSubview(self.progressView!)
        })
    }
    
    func updateProgress(newProgress: Float) {
        dispatch_async(dispatch_get_main_queue(), {
            print("progress: \(newProgress)")
            if (self.progressView?.progress) != nil {
                self.progressView!.progress = newProgress
            }
        })
    }
    
    //MARK: Error Message
    func displayError(errorCode: ErrorCode) {
        alertView?.dismissViewControllerAnimated(true, completion: nil)
        seedLineTF.userInteractionEnabled = true
        seedLineTF.resignFirstResponder()
        
        let title = "Cannot Generate Rap"
        var message = "Error. Could not generate rap. Please contact us at rapgenerator@gmail.com."
        
        if errorCode == .ServerDown {
            message = "RapGen server is not online. Please contact us at rapgenerator@gmail.com."
        } else if errorCode == .NoInternet {
            message = "No Internet Connection available"
        } else if errorCode == .Artist {
            message = "RapGen is still learning to rap like \(settings.rapType). Finna drop soon"
        } else if errorCode == .ServerOverload {
            message = "RapGen is over capacity. Please try again in a few minutes."
        } else if errorCode == .Char {
            message = "Please enter valid characters in your sick line."
        }
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: .Default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(okAction)
        
        self.presentViewController(alert,
                                   animated: true,
                                   completion: nil)
    }
    
    //do nothing
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

enum ErrorCode {
    case Error, ServerDown, NoInternet, Artist, ServerOverload, Char
}