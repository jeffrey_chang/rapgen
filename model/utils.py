import numpy as np

# Set of all characters in training data.
# Two special characters at the end, `B` and `E`, for beginning and end of a verse

VERSE_START = 'B'
VERSE_END   = 'E'
chars = '\n !"#$%&\'()*+,-./0123456789:;?[]abcdefghijklmnopqrstuvwxyz' + VERSE_START + VERSE_END

char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))
n_chars = len(chars)

rappers = ['Drake', 'Dr. Dre', 'Eminem', 'Kanye West', 'Kendrick Lamar', 'The Notorious B.I.G.']
rapper_indices = dict((c, n_chars + i) for i, c in enumerate(rappers))
indices_rapper = dict((n_chars + i, c) for i, c in enumerate(rappers))
n_rappers = len(rappers)

null_rapper    = 'RapGen'
default_rapper = 'Dr. Dre'

# We concatenate together the character vector and rapper vector as
# the input to our model.
n_features = n_chars + n_rappers

def vectorify(sentence, rapper, batch_size):
    # turn a string and a rapper into LSTM-readable numpy array
    x = np.zeros((batch_size, len(sentence), n_features))
    for t,char in enumerate(sentence):
        x[:, t, char_indices[char]] = 1
        x[:, t, rapper_indices[rapper]] = 1
    return x

# Simple generate text from seed
def generate(model, seed_text='\n', rapper='Eminem', diversity=0.5, batch_size=None, length=1000):
    if not batch_size:
        raise Exception("Need to specify batch size in order to generate")

    def sample(a):
        # sample an index from a probability array
        a = np.log(a) / diversity
        a = np.exp(a) / np.sum(np.exp(a))
        return np.argmax(np.random.multinomial(1, a, 1))

    model.reset_states()
    generated = seed_text
    next_char = seed_text

    for i in range(length):
        x = vectorify(next_char, rapper, batch_size)
        preds = model.predict(x, verbose=0, batch_size=100)[0]
        next_index = sample(preds)
        next_char = indices_char[next_index]
        generated += next_char

    return generated
