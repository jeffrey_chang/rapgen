import json
import numpy as np
import h5py
import theano
import theano.tensor as T
import keras

def make_function(f, reshape=False):
    'Compile a theano function for the function `f`'
    X = T.vector()
    if reshape:
        X = X.reshape((1, -1))
    Y = f(X)
    return theano.function([X], Y, allow_input_downcast=True)

sigmoid = make_function(keras.activations.hard_sigmoid)
tanh    = make_function(keras.activations.tanh)
softmax = make_function(keras.activations.softmax, reshape=True)

# def sigmoid(x):
    # """Return an piecewise linear approximation of the sigmoid function:
    # sigmoid(x) = 1 / (1 + np.exp(-x))'
    # """
    # return np.clip((0.2*x) + 0.5, 0, 1)

# def softmax(x):
    # "Returns softmax function of x, calculated in a numerically stable fashion"
    # e_x = np.exp(x - x.max(axis=1))
    # out = e_x / e_x.sum(axis=1)
    # return out

# def tanh(x):
    # return np.tanh(x)

def init(*shape):
    return np.zeros(tuple(shape), dtype='float32')

class Layer(object):
    def __init__(self, xdim, **kwargs):
        self.xdim = xdim
        self.ydim = kwargs.get('output_dim')
        if not self.ydim:
            self.ydim = self.xdim

    @staticmethod
    def init_layer(xdim, class_name, **kwargs):
        return globals()[class_name](xdim=xdim, **kwargs)

    def __call__(self):
        'Given x as the input to the layer, what is the output?'
        pass

    def set_weights(self, weights):
        'Sets the weights of layer from a list of hdf5 datasets'
        if not hasattr(self, 'weights'):
            return

        for i,weight in enumerate(weights):
            if self.weights[i].shape != weight.shape:
                raise Exception('Shape of new weight ' + str(weight.shape) + 
                        'not compatible with current shape ' + str(self.weights[i].shape))

            self.weights[i] = np.asarray(weight)

    def reset_states(self):
        'Only implemented for recurrent layers with state (like LSTM)'
        pass


class Dropout(Layer):
    def __init__(self, p, **kwargs):
        super(Dropout, self).__init__(**kwargs)
        self.p = p

    def __call__(self, x):
        return x * self.p


class Activation(Layer):
    def __init__(self, **kwargs):
        super(Activation, self).__init__(**kwargs)

    def __call__(self, x):
        return softmax(x.reshape((1,-1)))

class Dense(Layer):
    def __init__(self, **kwargs):
        super(Dense, self).__init__(**kwargs)
        W = init(self.xdim, self.ydim)
        b = init(self.ydim)
        self.weights = [W, b]

    def __call__(self, x):
        W,b = self.weights
        y = np.dot(x, W) + b
        return y

class LSTM(Layer):
    def __init__(self, **kwargs):
        super(LSTM, self).__init__(**kwargs)

        xdim = self.xdim
        ydim = self.ydim

        # Weight matrices
        W_i = init(xdim, ydim) # input gates
        U_i = init(ydim, ydim)
        b_i = init(ydim)
        W_f = init(xdim, ydim) # forget gates
        U_f = init(ydim, ydim)
        b_f = init(ydim)
        W_c = init(xdim, ydim) # cell gates
        U_c = init(ydim, ydim)
        b_c = init(ydim)
        W_o = init(xdim, ydim) # output gates
        U_o = init(ydim, ydim)
        b_o = init(ydim)

        # Hidden states
        h = init(ydim)
        c = init(ydim)

        self.weights = [W_i, U_i, b_i,
                        W_c, U_c, b_c,
                        W_f, U_f, b_f,
                        W_o, U_o, b_o]

        self.states = [h, c]

    def __call__(self, x):
        # Calculate gate activations
        # (sigmoid is `inner activation` and tanh is `activation` functions)
        W_i, U_i, b_i, W_c, U_c, b_c, W_f, U_f, b_f, W_o, U_o, b_o = self.weights
        h,c = self.states

        i  = sigmoid(np.dot(x, W_i) + np.dot(h, U_i) + b_i)
        f  = sigmoid(np.dot(x, W_f) + np.dot(h, U_f) + b_f)
        c_ = tanh(np.dot(x, W_c) + np.dot(h, U_c) + b_c)
        o  = sigmoid(np.dot(x, W_o) + np.dot(h, U_o) + b_o)

        # Calculate hidden states
        c = f * c + i * c_
        h = o * tanh(c)

        # Update hidden states
        self.states = [h, c]

        # Output to next layer is `h` (hidden layer) not `c` (cell state)
        return h

    def reset_states(self):
        for state in self.states:
            state *= 0

class Model(object):
    def __init__(self, json_string):
        'Initialize model from json_string describing architecture'
        j = json.loads(json_string)

        # keep track of current size of vector passed between layers
        self.input_shape = j['config'][0]['config']['batch_input_shape']
        width = self.input_shape[2]

        # add layers one by one
        self.layers = []
        for l in j['config']:
            class_name = l['class_name']
            layer = Layer.init_layer(xdim=width, class_name=class_name, **dict(l['config']))
            self.layers.append(layer)
            width = layer.ydim

    def load_weights(self, filename):
        'Load model weights from `filename`, an h5 file'
        with h5py.File(filename, mode='r') as f:
            n_layers = f.attrs['nb_layers']
            for i in range(n_layers):
                layer = f['layer_%d' % i]
                n_params = layer.attrs['nb_params']
                weights = [layer['param_%d' % j] for j in range(n_params)]
                self.layers[i].set_weights(weights)

    def predict(self, X, batch_size=None):
        """Given a sentence X, predict probability distribution over next char.'
        X is a numpy array of dimention (time, self.n_features), where time is the
        length of the sample, or number of characters.

        Returns array of dimentions (self.n_features), representing odds of each char.
        """
        # note: hacky way to deal with different shapes for now. Right now it's passing
        # in 3D X's, containing multiple samples in a batch size. I don't know if the
        # matrix multiplication is set up right for that.
        if len(X.shape) == 3:
            X = X[0]

        # Model was trained on 32-bit floats so we should respect that.
        X = X.astype('float32')
        for t in range(X.shape[0]):
            x = X[t]
            y = self._predict(x)
        return [y]

    def _predict(self, x):
        'Internal function. Given input x, what is output y? Feed through each layer.'
        for layer in self.layers:
            x = layer(x)
        return x

    def reset_states(self):
        for layer in self.layers:
            layer.reset_states()

def model_from_json(json_string):
    return Model(json_string)
