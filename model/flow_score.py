# Calculate the "flow score" of a line of rap
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM, SimpleRNN
from keras.callbacks import Callback
from keras.optimizers import RMSprop
import numpy as np
import random
import itertools
import string

path = "data/dre-lyrics.txt"
text = open(path).read().lower()
print 'corpus length: %d' % len(text)

filler = '_'
chars = set(text + filler)
print 'total chars: %d' % len(chars)
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

real_size = 100
fake_size = 100
batch_size = real_size + fake_size
maxlen = 100
minlen = 10
n_features = len(chars)
nb_epoch = 10

lines = np.array([line for line in text.splitlines() 
                       if minlen <= len(line) <= maxlen])
n_samples = len(lines)
print 'number of training samples: %d' % n_samples
reals = itertools.cycle(lines[np.random.permutation(n_samples)])
fakes = itertools.cycle(lines[np.random.permutation(n_samples)])

punctuation = '!"#$%&()*+,-./:;<=>?@[\\]^_{|}~'
clean_text = text.translate(None, punctuation)
vocab = clean_text.split()
print 'size of vocab: %d ' % len(vocab)

def vectorify(text):
    "From python string into one-hot to feed into model"
    # pad with filler till maxlen
    text = (text + filler*maxlen)[:maxlen]
    x = np.zeros((maxlen, n_features), dtype=np.bool)
    for t in range(maxlen):
        x[t, char_indices[text[t]]] = 1
    return x

def wordify(array):
    "From one-hot numpy array into python string"
    indices = array.argmax(1)
    chars = [indices_char[i] for i in indices]
    text = ''.join(chars).strip(filler)
    return text

def fakify(line, times=5):
    words = []
    for _ in range(random.randrange(5, 15)):
        words.append(random.choice(vocab))
    return ' '.join(words)
    # "fakify_ times times"
    # for _ in range(times):
        # line = fakify_once(line)
    # return line

def fakify_once(line):
    "Remove or add random word to line to change its flow"
    choice = random.choice(["insert", "delete"])
    words = line.split()
    if len(words) <= 5:
        choice = "insert"
    if choice == "insert":
        word = random.choice(vocab)
        index = random.randrange(len(words) + 1)
        words.insert(index, word)
        return ' '.join(words)
    else:
        index = random.randrange(len(words))
        words.pop(index)
        return ' '.join(words)

def batch_generator():
    while True:
        real = itertools.islice(reals, real_size)
        real = np.array([vectorify(line) for line in real])
        fake = itertools.islice(fakes, fake_size)
        fake = np.array([vectorify(fakify(line)) for line in fake])
        X = np.concatenate((real,fake))
        y = np.array([1]*(real_size) + [0]*(fake_size))

        #shuffle up order of things in batch
        order = np.random.permutation(batch_size)
        X = X[order]
        y = y[order]
        yield X,y
batches = batch_generator()
X,y = batches.next()

if __name__=="__main__":
    print 'Building model...' 
    model = Sequential()
    model.add(LSTM(512, return_sequences=True, input_shape=(None, n_features)))
    model.add(Dropout(0.5))
    model.add(LSTM(512, return_sequences=False, input_shape=(None, n_features)))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer=RMSprop(lr = 0.01))

    # the pretrained weight matrices lack the filler char so theyre the wrong size
    def fix(weight):
        i = char_indices[filler]
        weight = np.asarray(weight, dtype=weight.dtype)
        if weight.shape[0] == n_features - 1:
            return np.insert(weight, i, 0, axis=0)
        else:
            return weight

    # we wanna load pretrained layers as a good starting point
    import h5py
    filepath = "rapGenServer/model_files/dre.h5"
    with h5py.File(filepath, mode='r') as f:
        for k in [0,2]: # Layer numbers we wanna copy weights from
            g = f["layer_%d" % k]
            weights = [g["param_%d" % p] for p in range(g.attrs['nb_params'])]
            weights = [fix(weight) for weight in weights]
            model.layers[k].set_weights(weights)

    model.fit_generator(batches, samples_per_epoch=n_samples, nb_epoch=nb_epoch)
