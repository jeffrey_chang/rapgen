import string
import json
import requests
import random
import os
import collections
from itertools import izip

def memoize(f):
    """ Memoization decorator for a function taking a single argument """
    class memodict(dict):
        def __missing__(self, key):
            ret = self[key] = f(key)
            return ret 
    return memodict().__getitem__

def all_rhymes(word):
    'Call datamuse to find all rhymes to a word.'
    url = 'https://api.datamuse.com/words'
    try:
        response = requests.get(url, params={'rel_rhy': word, 'max': 1000}, timeout=1)
        data = response.json()
        rhymes = [entry['word'] for entry in data]
    except:
        print "Rhyme API timed out"
        rhymes = []
        
    return rhymes 


def make_vocab(filename):
    filepath = os.path.join(os.path.dirname(__file__), 'data', filename)
    text = open(filepath).read().lower()
    lines = text.splitlines()
    good_lines = [line.strip(string.punctuation) for line in lines]
    last_words = [line.split()[-1] for line in good_lines if len(line.split())]
    last_bigrams = [' '.join(line.split()[-2:]) for line in good_lines if len(line.split()) > 1]
    vocab = collections.Counter(last_words + last_bigrams)
    return vocab

training_file = 'Eminem.txt'
vocab = make_vocab(training_file)

@memoize
def make_choices(target):
    'Return dictionary of rhyming words to their frequencies in the training data'
    rhymes = all_rhymes(target)
    choices = [(word,freq) for (word,freq) in vocab.iteritems() if word in set(rhymes)]
    if not choices:
        choices = [('', 1)]
    total_freq = sum(freq for word,freq in choices)
    choices = [(word,1.0*freq/total_freq) for word,freq in choices]
    return choices

def pick_rhyme(target):
    'Sample a rhyming word to `target` based on its frequency in training data.'
    x = random.random()
    for word,freq in make_choices(target):
        x -= freq
        if x < 0:
            return word

dirtyWords = {'fuck': 'fudge', 
        'bitch': 'bizzle', 
        'nigg': 'hitt',
        'shit': 'snap',
        'dick': 'stick',
        'whore': 'bore',
        'ho ': 'show',
        'ass': 'cat',
        'pussy': 'sissy',
        'fudgeer': 'fudger',
        'fudgein': 'freakin',
        'cunt': 'blunt',
        'penis': 'genius',
        'porn': 'pork'}

def cleanify(text):
    'Clean the bad words in text.'
    for key, value in dirtyWords.iteritems():
        text = text.replace(key, value)
    return text
