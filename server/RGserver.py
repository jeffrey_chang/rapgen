#!/usr/bin/env python
"""Start a rapgen server.

Usage: 
    RGserver.py [-p PORT] [-n NUM_PROCESSES] [--only-dispatch]

Options:
    -h --help         Show this screen.
    -p PORT           Port number of main server [default: 8000].
    -n NUM_PROCESSES  Number of sub-processes to spawn [default: 2].
    --only-dispatch   Don't start child processes for rap

"""
from docopt import docopt
from twisted.internet import reactor
from twisted.internet.protocol import ClientFactory, ServerFactory
from twisted.protocols.basic import LineReceiver
import multiprocessing
import sys

from rap_protocol import RapProtocol, RapFactory
from simple_server import start_simple_server

def debug(s):
    print 'RG server:', s

class Relay(LineReceiver):
    "Send generated rap from SimpleServer back to client."
    def connectionMade(self):
        # Client may have lost connection already
        if not self.factory.client.peer:
            self.transport.loseConnection()
            return

        self.factory.client.server = self
        self.delimiter = '\n'

        # Relay client's request to actual generating server
        for line in self.factory.client.lines:
            self.sendLine(line)

    def lineReceived(self, line):  
        self.factory.client.sendLine(line)

    def connectionLost(self, reason):
        self.factory.client.transport.loseConnection()

class RelayFactory(ClientFactory):
    def __init__(self, client):
        self.protocol = Relay
        self.client = client


class DispatchProtocol(RapProtocol):
    """Rap server that generates rap by dispatching to available SimpleServers.
    It can take on multiple clients, but rejects clients if all SimpleServers are taken.
    """
    def __init__(self):
        RapProtocol.__init__(self)
        self.server_port = None # exists iff a server was available
        self.server = None      # exists iff successfully connected to server

    def connectionLost(self, reason):
        RapProtocol.connectionLost(self, reason)
        if self.server_port:
            self.factory.availables[self.server_port] = True
        if self.server:
            self.server.transport.loseConnection()

    def send_rap(self, params):
        'Initiate connection to an available SimpleServer and defer rap generation to it'
        for port,available in self.factory.availables.iteritems():
            if available:
                self.server_port = port
                self.factory.availables[port] = False
                reactor.connectTCP('localhost', port, RelayFactory(self))
                return
        else:
            debug('All servers busy, closing connection with %s' % self.peer)
            self.busy_reject()

class DispatchFactory(RapFactory, ServerFactory):
    def __init__(self, port, n_processes):
        RapFactory.__init__(self)
        self.protocol = DispatchProtocol

        ports = range(port+1, port+n_processes+1)
        self.availables = {port: True for port in ports}


if __name__ == '__main__':
    try:
        arguments   = docopt(__doc__)
        port        = int(arguments['-p'])
        n_processes = int(arguments['-n'])
    except ValueError:
        print 'Arguments must be integers.'
        sys.exit(1)

    if not arguments['--only-dispatch']:
        # Start child processes that are the actual simple servers
        for i in range(n_processes):
            p = multiprocessing.Process(name='Worker %d' % i, 
                    target=start_simple_server, args=(port + i + 1, ))
            p.start()

    # Start dispatch server to link up clients with those servers
    debug('Starting front server at localhost:%d' % port)
    reactor.listenTCP(port, DispatchFactory(port, n_processes))
    reactor.run()
