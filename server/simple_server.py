from twisted.internet import reactor
from twisted.internet import threads
from twisted.internet import task
from twisted.protocols.basic import LineReceiver

import rap_protocol

def debug(x):
    print 'RG server: ' + x

class SimpleServer(rap_protocol.RapProtocol):
    """Simple server that generates the rap in a separate thread.
    It can only take on one client at a time.
    It should run in its own process. """
    def connectionMade(self):
        rap_protocol.RapProtocol.connectionMade(self)
        if self.factory.num_conns != 1:
            debug('More than one client attempted connecting.')
            self.busy_reject()
            return

    def send_rap(self, params):
        self.send_GOODHEAD(params)
        reactor.callInThread(self.gen_loop, params)

    def gen_loop(self, params):
        'Called in a thread, slowly gets rap from generator one line at a time'
        rap = self.factory.gen.generate_generator(**params)
        for line in rap:
            if self.is_stop_signalled():
                debug('(gen-thread) got stop signal')
                reactor.callFromThread(self.finish_with_error)
                return
            debug('(gen-thread) generated ' + repr(line))
            reactor.callFromThread(self.sendLine, line)

        reactor.callFromThread(self.finish)

class SimpleFactory(rap_protocol.RapFactory):
    def __init__(self):
        rap_protocol.RapFactory.__init__(self)
        self.protocol = SimpleServer

        from model.generate import Generator
        #  from rap_protocol import FakeGenerator as Generator
        self.gen = Generator()
        debug('Compiling functions...')
        self.gen.generate()
        debug('Functions compiled.')

def start_simple_server(port):
    debug('Process begun.')

    global debug
    def debug(x):
        print 'port %d: %s' % (port,x)
    rap_protocol.debug = debug

    reactor.listenTCP(port, SimpleFactory())
    debug('Starting server on localhost:%d, use <Ctrl-C> to stop' % port)

    # We want reactor to "wake up" every second
    # to remember to send its shit over
    def do_nothing():
        pass
    task.LoopingCall(do_nothing).start(1)

    reactor.run()

if __name__=='__main__':
    start_simple_server(port=8001)
