from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
import threading

from model import utils

LISTENIN  = 'listenin for the rest of the message'
PARSIN    = 'parsin the message'
RAPPIN    = 'busy rappin right now bruh'
TRIPPIN   = 'some kinda error'
DONE      = 'we outta here we generated'

def debug(x):
    print 'RG server: ' + x

class FakeGenerator(object):
    "this way we don't have to load keras during testing"
    def __init__(self):
        pass

    def generate(self):
        pass

    def generate_generator(self, **kwargs):
        for i in [8,7,6,5,4,3,2,1]:
            # kill some time
            for x in xrange(30000000):
                x = x * 2
            yield 'line numba %d' % i

class ClientDisconnected(Exception):
    pass

class RapProtocol(LineReceiver):
    """Implements the rap protocol."""
    def __init__(self):
        self.delimiter = '\n'
        self.lines = []
        self.state = LISTENIN
        self.lock = threading.Lock()
        self.stop_signal = 'GO'
        self.peer = None

    def connectionMade(self):
        self.factory.num_conns += 1
        peer = self.transport.getPeer()
        self.peer = '%s:%d' % (peer.host, peer.port)

        debug('Connection made with %s.\t(%d connections total now)'
                % (self.peer, self.factory.num_conns))

    def connectionLost(self, reason):
        self.factory.num_conns -= 1
        peer = self.peer
        self.peer = None

        debug('Connection lost from %s.\t(%d connections total now)'
                % (peer, self.factory.num_conns))

        if self.state == RAPPIN:
            self.send_stop_signal()

    def lineReceived(self, line):
        if self.state == LISTENIN:
            self.lines.append(line.strip())
        if self.state == RAPPIN:
            if line == "JK":
                self.send_stop_signal()
        if self.ready_to_generate():
            self.parse_and_generate()

    def send_stop_signal(self):
        debug('sending stop signal')
        with self.lock:
            self.stop_signal = 'STOP'
        if self.peer:
            self.transport.loseConnection()

    def is_stop_signalled(self):
        with self.lock:
            return self.stop_signal == 'STOP'

    def ready_to_generate(self):
        return self.state == LISTENIN and len(self.lines) == 3

    def parse_and_generate(self):
        self.state = PARSIN
        good,params = self.parsify()
        if good:
            debug('request was valid, starting rap generation.')
            self.state = RAPPIN
            self.send_rap(params)
        else:
            debug('request was invalid, sending back error message.')
            self.state = TRIPPIN
            self.send_BRUH(params)

    def send_rap(self, params):
        """Varies depending on whether the server will link up with another server
        and relay its rap, or generate the rap itself."""
        raise NotImplementedError

    def send_GOODHEAD(self, params):
        self.sendLine("HERES YO RAP DAWG, ITS %d LINES" % params['n_lines'])

    def send_BRUH(self, params):
        self.sendLine("BRUH YOU GOOFED")
        self.sendLine(params['error'])
        self.transport.loseConnection()

    def busy_reject(self):
        self.sendLine('DAMMIT')
        self.sendLine('Shit dude sorry all our servers are taken')
        self.transport.loseConnection()

    def process_seed_text(self, text):
        for c in text:
            if c not in utils.chars:
                raise Exception("bad char: " + c)
        return text.lower()

    def parsify(self):
        good = True
        params = dict()

        # for faster debugging
        if self.lines[0] == 'haobling':
            self.lines = ['GIMME A RAP', 'testing seed', 'false 0.7 Drake']

        try:
            seed_text = self.process_seed_text(self.lines[1])
            words     = self.lines[2].split()
            clean     = words[0]
            diversity = words[1]
            rap_type  = ' '.join(words[2:]) 
            # TODO: edit protocol so space is no longer delimiter between arguments

            if self.lines[0] != "GIMME A RAP":
                raise Exception("bad command")
            if rap_type not in utils.rappers and rap_type != utils.null_rapper:
                raise Exception("bad rapper")
            if float(diversity) < 0.00000001:
                raise Exception("Diversity cannot be 0")

            params['seed_text'] = seed_text
            params['clean']     = not (clean == 'false')
            params['diversity'] = float(diversity)
            params['rapper']    = rap_type
            params['n_lines']   = 8    # This is a preset, might change later.
            good = True

        except Exception as e:
            print e.message
            if e.message.startswith('too many values') or e.message.startswith('need more than') or e.message.startswith('list index'):
                params['error'] = "Ya gotta gimme three values on the third line dude" # need to fix
            elif e.message.startswith('could not convert'):
                params['error'] = "The high factor's gotta be a float man"
            elif e.message.startswith('bad command'):
                params['error'] = "Yo command don't make no sense"
            elif e.message.startswith('bad rapper'):
                params['error'] = "RapGen don't how to rap like %s yet. Finna drop soon" % rap_type
            elif e.message.startswith('bad char'):
                params['error'] = "The f does %s mean? nigga you trippin" % repr(e.message[10:])
            else:
                debug(e.message)
                params['error'] = e.message
            good = False

        return good,params

    def finish(self, success=None):
        debug('generation finished successfully.')
        self.state = DONE
        self.transport.loseConnection()

    def finish_with_error(self, failure=None):
        if failure:
            failure.trap(ClientDisconnected)
        debug('generation interrupted.')
        self.state = TRIPPIN
        self.transport.loseConnection()

class RapFactory(Factory):
    def __init__(self):
        self.num_conns = 0
        self.gen = None
