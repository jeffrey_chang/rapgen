package com.example.timothy.rapgenandroid.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.timothy.rapgenandroid.R;

import java.net.InetAddress;


/**
 * A simple {@link Fragment} subclass.
 */
public class GenerateFragment extends Fragment 
    implements GenerateTask.UI {
    public static final String ARG_PAGE = "ARG_PAGE";
    private String TAG = "GenerateFragment"; // for logging

    private EditText userLineET; //where to enter rap seed
    private EditText lyricsET;  //where lyrics display
    private ProgressDialog pd;   //progress dialog to notify user of problems
    private Button saveButton;   //save button for user to save rap
    private Button shareButton;
    private GenerateTask task; // AsyncTask where generating magic happens

    public interface SavedRaps { // implemented by this guy's activity
        void addRap(String title, String lyrics);
    }
    private SavedRaps savedRaps;

    private String lyrics = "";

    /* ---------------------
     * INITIALIZATION METHODS
     * ---------------------
     */

    public static GenerateFragment newInstance(int page) {
        //fragments use newInstance for initialization (not constructors)
        //http://stackoverflow.com/questions/10450348/do-fragments-really-need-an-empty-constructor
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        GenerateFragment fragment = new GenerateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            savedRaps = (SavedRaps) getActivity();
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + " must implement SavedRaps.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_generate, container, false);


        //progressDialog setup
        pd = new ProgressDialog(getActivity());
        pd.setTitle("Generating Rap...");
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setIndeterminate(false);
        pd.setProgress(0);

        pd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        task.cancel(true);
                        dialog.dismiss();
                    }
                });
        pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                task.cancel(true);
            }
        });

        //Save button setup
        saveButton = (Button) view.findViewById(R.id.saveButton);
        //saveButton.setVisibility(View.INVISIBLE); //rap hasn't been generated yet
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAlertForTitle();
            }
        });

        //Share button setup
        shareButton = (Button) view.findViewById(R.id.shareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getActivity(), "RapGen is still in beta so the share feature is disabled.", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });

        //Lyrics text area setup
        lyricsET = (EditText) view.findViewById(R.id.lyrics);
        final KeyListener lyricsKeyListener = lyricsET.getKeyListener();
        lyricsET.setCursorVisible(false);
        lyricsET.setKeyListener(null);

        //Edit lyrics setup
        final Button editButton = (Button) view.findViewById(R.id.editButton);
        final View.OnClickListener clickedListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setPressed(true);
                editButton.setTextColor(Color.parseColor("#f1c40f"));
                lyricsET.setCursorVisible(true);
                lyricsET.setKeyListener(lyricsKeyListener);
                saveButton.setEnabled(false);
                saveButton.setVisibility(View.INVISIBLE);
                shareButton.setEnabled(false);
                shareButton.setVisibility(View.INVISIBLE);
            }
        };
        editButton.setOnClickListener(clickedListener);

        // set up the "done" button on the keyboard to cause rap generation request to send.
        userLineET = (EditText) view.findViewById(R.id.userLine);
        userLineET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //hide keyboard
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    beginGenerate();
                }
                return true; //handled
            }
        });

        userLineET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    InputMethodManager imm = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });


        lyricsET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //hide keyboard
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        editButton.setPressed(false);
                        editButton.setTextColor(Color.BLACK);
                        lyricsET.setCursorVisible(false);
                        lyricsET.setKeyListener(null);
                        shareButton.setEnabled(true);
                        shareButton.setVisibility(View.VISIBLE);
                        saveButton.setEnabled(true);
                        saveButton.setVisibility(View.VISIBLE);
                    }
                });
                return true; //handled
            }
        });

        return view;
    }

    // Initiate the Async thing that connects to server and sets up rap generation
    private void beginGenerate() {
        // User can no longer enter text during generation
        userLineET.setEnabled(false);

        // Clear any text currently there
        lyricsET.setText("");

        pd.setProgress(0);
        // Show the spinny thingy
        pd.show();

        //reset lyrics
        lyrics = "";

        Params params = getParams();
        task = new GenerateTask(this);
        task.execute(params.userLine, params.clean, params.highFactor, params.type);
    }

    public void addRapLine(String line) {
        lyrics = line + " / " + lyrics;
        lyricsET.setText(lyrics);
    }

    public void setProgress(double progress) {
        pd.setProgress((int) (progress * 100));
    }

    public void rapFinished(boolean success) {
        Log.v("RG", success ? "Rap finished successfully" : "Rap finished unsuccessfully");
        if(success) {
            pd.setProgress(100);

            String toSet = lyrics.substring(0, lyrics.length() - 4); //??

            lyricsET.setText(toSet);

            Log.v("RG", "lyrics begin");
            Log.v("RG", toSet);
            Log.v("RG", "lyrics end");

            // User can save if it was good
            saveButton.setEnabled(true);
            saveButton.setVisibility(View.VISIBLE);
        }

        // User can enter raps again
        userLineET.setEnabled(true);

        pd.setProgress(0);
        // Get rid of the spinny thingy
        pd.dismiss();

    }

    // ---------------------
    // Saving raps
    // ---------------------

    //creates an alert so that user can enter metadata for the saved rap
    private void saveAlertForTitle() {
        //build a new alert
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Save Rap");

        // Set up the input
        final EditText input = new EditText(getActivity());
        input.setHint("Enter title for this rap");
        builder.setView(input);

        // Set up the alert buttons
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //get title from user input
                String title = input.getText().toString();
                title = title.length() > 0 ? title : "Untitled";
                save(title);
                Toast toast = Toast.makeText(getActivity(),
                        "Rap saved!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    //save method called by saveAlertForTitle(). saves rap after user enters metadata
    private void save(String title) {
        savedRaps.addRap(title, lyrics);

        //Make the save button disappear cuz we don't want to save twice
        saveButton.setEnabled(false);
        saveButton.setVisibility(View.INVISIBLE);
    }

    /* ---------------------
     * UTILITY METHODS
     * ---------------------
     */

    private class Params {
        public String userLine;
        public String clean;
        public String highFactor;
        public String type;

        public void debugPrint() {
            Log.d(TAG, "Set parameters below:");
            Log.d(TAG, "userLine: " + userLine);
            Log.d(TAG, "clean: " + clean);
            Log.d(TAG, "highFactor: " + highFactor);
            Log.d(TAG, "type: " + type);
        }
    }

    // Returns user preferences and line in a Params object.
    private Params getParams() {
        Params params = new Params();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(
                getActivity().getBaseContext());

        try {
            params.userLine = userLineET.getText().toString();
            params.clean = String.valueOf(sharedPref.getBoolean("cleanKey", true));
            params.highFactor = processHigh(sharedPref.getString("highFactorKey", "0.50"));
            params.type = sharedPref.getString("typeKey", "Eminem");
            params.debugPrint();
        } catch(Exception e) {
            Log.d(TAG, "Error caught while processing preferences:");
            Log.d(TAG, e.toString());
        }

        return params;
    }

    // Process diversity [1, 100] --> [0.1, 3.0]
    private String processHigh(String highFactorStr) {
        double highFactor= ((double) Integer.parseInt(highFactorStr)) / 100;
        if (highFactor <= 0.5) {
            highFactor = 0.1 + 1.2 * highFactor;
        } else {
            highFactor = -0.3 + 2.0 * highFactor;
        }
        return Double.toString(highFactor);
    }

    public void displayError(final String title, final String message) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                //Context context = getApplicationContext();
                new AlertDialog.Builder(getActivity())
                        .setTitle(title)
                        .setMessage(message)
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();

            //after error is displayed, edittext/generate can be renabled for user to generate again
            userLineET.setEnabled(true);
            }
        });
    }

    //Returns whether there's an internet connection on the phone.
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = connectivityManager.getActiveNetworkInfo();
        return network != null && network.isConnected();
    }

    //Returns whether rapgen is up.
    private boolean isServerUp() {
        int timeout = 1000;
        try {
            return InetAddress.getByName(GenerateTask.SERVER_IP).isReachable(timeout);
        } catch (Exception e) {
            // Failed to establish connection with server or otherwise some bad shit
            e.printStackTrace();
            return false;
        }
    }    // Check if we can begin generatin'

    public boolean checksPass() {
        if(!isNetworkAvailable()) {
            displayError("No internet connection",
                    "Please check yo internet connection dawg. Gotta have internet to work!");
            return false;
        }
        if (!isServerUp()) {
            displayError("Server down",
                    "Our server is down. Please contact us at rapgenerator@gmail.com");
            return false;
        }
        return true;
    }
}
