package com.example.timothy.rapgenandroid.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.timothy.rapgenandroid.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavedFragment extends ListFragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private HashMap<String, String> raps;
    //TODO: put it in a list cuz hashtables mess up the order

    public static SavedFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        SavedFragment fragment = new SavedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_saved, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadRaps();
    }

    void writeRapsToFile() {
        File file = new File(getActivity().getFilesDir(), "savedRaps");
        try {
            //using ObjectOutputStream to write HashMap object to internal storage file
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(raps);
            objectOutputStream.close();
        } catch (Exception e) {
            //TODO: notify user here with alert dialog if error
            Log.v("RG", e.toString());
            Log.v("RG", "Failed to save (write)");
        }
    }

    void readRapsFromFile() {
        File file = new File(getActivity().getFilesDir(), "savedRaps");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object obj = objectInputStream.readObject();
            raps = (obj instanceof HashMap) ? (HashMap<String, String>) obj : null;
            objectInputStream.close();
        } catch (Exception e) {
            //TODO: notify user here with alert dialog if error
            Log.v("RG", e.toString());
            Log.v("RG", "Failed to save (read)");
        }
        if (raps == null)
            raps = new HashMap<String, String>();
    }

    public void loadRaps() {
        readRapsFromFile();
        updateSaved();
    }

    public void addRap(String title, String lyrics) {
        raps.put(title, lyrics);
        writeRapsToFile();
        updateSaved();
    }

    void removeRap(String title) {
        raps.remove(title);
        writeRapsToFile();
        updateSaved();
    }

    class SavedAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<String> list = new ArrayList<>();
        private Context context;

        public SavedAdapter(Context context, String[] list) {
            this.context = context;
            this.list = new ArrayList<>(Arrays.asList(list));
        }
        @Override
        public int getCount() { return list.size(); }
        @Override
        public Object getItem(int pos) { return list.get(pos); }
        @Override
        public long getItemId(int pos) { return list.get(pos).hashCode(); }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater)
                        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.saved_rap_item, null);
            }

            // Handle rap title
            TextView titleTV = (TextView)view.findViewById(R.id.title);
            titleTV.setText(list.get(position));
            titleTV.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    String title = list.get(position);
                    showRap(title, raps.get(title));
                }
            });

            // Handle delete button
            Button deleteBtn = (Button)view.findViewById(R.id.delete_btn);
            deleteBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    String title = list.get(position);
                    showDeleteDialog(title);
                }
            });
            return view;
        }
    }

    void showDeleteDialog(final String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeRap(title);
                Toast toast = Toast.makeText(getActivity(),
                        "Rap deleted!", Toast.LENGTH_SHORT);
                toast.show();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateSaved() {
        String[] titles = Arrays.copyOf(raps.keySet().toArray(), raps.size(), String[].class);
        SavedAdapter adapter = new SavedAdapter(getActivity().getApplicationContext(), titles);

        ListView listView = (ListView) getView().findViewById(R.id.listview);
        listView.setAdapter(adapter);
    }

    // Make new Fragment showing the rap
    private void showRap(String title, String lyrics) {
        RapDisplayFragment nextFrag = new RapDisplayFragment().newInstance(1, lyrics, title);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_out_right, R.anim.slide_in_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.container_saved, nextFrag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
