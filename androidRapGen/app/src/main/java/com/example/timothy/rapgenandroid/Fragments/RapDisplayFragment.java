package com.example.timothy.rapgenandroid.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.timothy.rapgenandroid.R;

//import android.support.v4.app.Fragment;

/**
 * This fragment displays saved raps
 */
public class RapDisplayFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PAGE = "ARG_PAGE";
    private static String rapLyrics;
    private static String rapTitle;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RapDisplayFragment.
     */
    public RapDisplayFragment newInstance(int page, String sentRap, String sentTitle) {
        RapDisplayFragment fragment = new RapDisplayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        rapLyrics = sentRap;
        rapTitle = sentTitle;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rap_display, container, false);
        TextView displayTitleTV = (TextView) view.findViewById(R.id.displayTitle);
        TextView displayLyricsTV = (TextView) view.findViewById(R.id.displayLyrics);

        displayTitleTV.setText(rapTitle);
        displayLyricsTV.setText(rapLyrics);

        Log.v("RG", displayLyricsTV.getText().toString());
        return view;
    }
}
