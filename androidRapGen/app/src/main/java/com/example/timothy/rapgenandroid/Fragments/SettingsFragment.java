package com.example.timothy.rapgenandroid.Fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.timothy.rapgenandroid.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static boolean clean;
    private static boolean rapType;
    private static double highFactor;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    public static SettingsFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(getActivity().getApplicationContext(), R.xml.preferences, false);
        initSummary(getPreferenceScreen());
    }

    private void initSummary(Preference p) {
        if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                initSummary(pGrp.getPreference(i));
            }
        } else {
            if (p instanceof ListPreference) {
                ListPreference listPref = (ListPreference) p;
                p.setSummary(listPref.getEntry());
            } else if (p instanceof EditTextPreference) {
                EditTextPreference editTextPref = (EditTextPreference) p;
                p.setSummary(editTextPref.getText());
            } else if (p instanceof highFactorSlider) {
                highFactorSlider slider = (highFactorSlider) p;
                int value = slider.getProgress();
                String highness = "Rehabbed";

                if(value > 80) {
                    highness = "Wasted";
                } else if(value > 60) {
                    highness = "Stoned";
                } else if(value > 40) {
                    highness = "In the Zone";
                } else if (value > 20) {
                    highness = "Detox";
                }

                p.setSummary(highness);
            }
        }
    }

   public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.v("RG", "Preference Change");
        if (key.equals("typeKey")) {
            Log.v("RG", "type");
            Preference typePref = findPreference(key);
            typePref.setSummary(sharedPreferences.getString(key, ""));
        } else if (key.equals("highFactorKey")) {
            Log.v("RG", "highFactor");
            Preference highFactorPreference = findPreference(key);
            highFactorPreference.setSummary(sharedPreferences.getString(key, ""));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }
}
