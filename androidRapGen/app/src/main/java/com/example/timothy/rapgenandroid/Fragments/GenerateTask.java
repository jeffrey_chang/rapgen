package com.example.timothy.rapgenandroid.Fragments;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Timothy on 8/2/16.
 */
public class GenerateTask extends AsyncTask<String, String, String> {
    public static final String SERVER_IP = "192.168.1.118";
    public static final int SERVER_PORT = 8000;
    private static final String TAG = "GenerateTask";
    private static final String HEADER = "GIMME A RAP";

    private int nLines, linesReceived;

    BufferedReader in;
    PrintWriter out;
    ListenState state;

    public enum ListenState {
        HEADERING, RELAYING, BRUHING, DAMMITING
    }

    public interface UI { // Implemented by GenerateFragment
        void addRapLine(String line);
        void setProgress(double progress);
        void displayError(final String title, final String message);
        void rapFinished(boolean success);
        boolean checksPass();
    }
    private UI ui;

    public GenerateTask(UI ui) {
        this.ui = ui;
        state = ListenState.HEADERING;
    }


    protected String doInBackground(String... params) {
        if(!ui.checksPass()){
            cancel(true);
            return null;
        }
        String command = HEADER + "\n"
              + params[0] + "\n" 
              + params[1] + " " 
              + params[2] + " " 
              + params[3];

        run(command);
        return null;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.d(TAG, "Task finished successfully.");
        ui.rapFinished(true);
    }

    protected void onCancelled(String good) {
        Log.d(TAG, "Task was cancelled.");
        //TODO: notify cancel
        ui.rapFinished(false);
    }

    private void run(String command) {
        Socket socket = null;
        try {
            InetAddress serverAddress = InetAddress.getByName(SERVER_IP);
            socket = new Socket(serverAddress, SERVER_PORT);
            out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            Log.d(TAG, "Socket and In/Out created.");

            // MOST IMPORTANT LINE OF THIS GODDAMN MESS HERE
            sendMessage(command);

            // MAIN LOOP
            String nextLine = "";
            while (!isCancelled() && nextLine != null) {
                nextLine = in.readLine();
                Log.d(TAG, "Received Message: " + nextLine);
                if (nextLine != null) {
                    publishProgress(nextLine);
                }
            }

        } catch (Exception e) {
            Log.d(TAG, "Error: " + e.toString(), e);

        } finally {
            try {
                out.flush();
                out.close();
                in.close();
                socket.close();
            } catch(Exception e) {
                Log.d(TAG, "Error: " + e.toString(), e);
                ui.displayError("Could not connect to server",
                        "Please try again. If issue persists, contact us at rapgenerator@gmail.com");
                cancel(true);
            }
        }
    }

    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        String line = values[0];

        if(state == ListenState.HEADERING) {
            processHeader(line);
        } else {
            processLine(line);
        }
    }

    private void processHeader(String line) {
        //theres gotta be a better way to do this...
        if(line.equals("BRUH YOU GOOFED")) {
            state = ListenState.BRUHING;
        } else if(line.equals("DAMMIT")) {
            state = ListenState.DAMMITING;
        } else if (line.substring(0, 23).equals("HERES YO RAP DAWG, ITS ")) {
            state = ListenState.RELAYING;
            String nLineStr = line.substring(23, line.length() - 6);
            Log.v(TAG, "NumStrings is: " + nLineStr);
            nLines = Integer.parseInt(nLineStr);
            linesReceived = 0;
        } else {
            // this shouldn't happen
            Log.d(TAG, "Unexpected header: " + line);
        }
    }

    private void processLine(String line) {
        if(state == ListenState.RELAYING) {
            ui.addRapLine(line);
            ui.setProgress((double) linesReceived/nLines);
            linesReceived++;
            if(linesReceived == nLines) {
                cancel(true);
                ui.rapFinished(true);
            }
        } else if(state == ListenState.BRUHING) {
            ui.displayError("Bad request", line);
            cancel(true);
        } else if(state == ListenState.DAMMITING) {
            ui.displayError("Too many clients", line);
            cancel(true);
        } else {
            // this shouldn't happen
            Log.d(TAG, "Unexpected state: " + state);
        }
    }

    public void sendMessage(String message) {
        if (out != null && !out.checkError()) {
            out.println(message);
            out.flush();
            Log.d(TAG, "Sent Message: " + message);
        }
    }

}
