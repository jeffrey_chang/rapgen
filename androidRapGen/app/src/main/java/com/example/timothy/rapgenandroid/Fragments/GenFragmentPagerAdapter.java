package com.example.timothy.rapgenandroid.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;

/**
 * Created by Timothy on 7/16/16.
 * Custom Fragment Pager Adapter to manage the three fragments in a tab layout
 */
public class GenFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = {"RapGen", "Saved Raps", "Settings"};
    private Context context;
    private String[] fragment_names = {GenerateFragment.class.getName(),
                                  ContainerFragment.class.getName(),
                                  SettingsFragment.class.getName()};

    //constructor
    public GenFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    //required method for pageradapter to know number of tabs
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return Fragment.instantiate(context, fragment_names[position]);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    //assigns page titles to tabs
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
