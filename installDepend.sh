#!/bin/sh
#set up AWS EC2 Instance
#sudo apt-get clean
#sudo apt-get update
#sudo apt-get install git
#sudo apt-get install python-pip
#sudo apt-get install python-dev
#sudo pip install uwsgi
#sudo pip install virtualenv virtualenvwrapper

#virtualenv venv
#source ~/venv/bin/activate

pip install django
pip install numpy
pip install scipy
pip install Cython

sudo apt-get install python-dev
sudo apt-get install libhdf5-dev

pip install h5py
pip install keras
pip install requests

sudo apt-get install libffi-dev libssl-dev
pip install requests[security]
